use Test2::V0;

use Museum::Rijksmuseum::Object;

my $api = Museum::Rijksmuseum::Object->new(
    key => 'testkey',
    culture => 'en',
);

# A basic search request
my $search = $api->_build_url({}, {q => 'myquery'});
my $search_uri = URI->new($search);

is($search_uri->scheme, 'https', 'URL scheme is https');
is($search_uri->path, '/api/en/collection', 'URL search path is correct');
is($search_uri->query_param('q'), 'myquery', 'URL search query param "q" is set');
is($search_uri->query_param('key'), 'testkey', 'URL search contains key');

my $detail = $api->_build_url({ object_number => 'ABC' }, {});
my $detail_uri = URI->new($detail);
is($detail_uri->path, '/api/en/collection/ABC', 'URL detail path is correct');
is([$detail_uri->query_param], ['key'], 'URL detail only has key param');
is($detail_uri->query_param('key'), 'testkey', 'URL detail contains key');

my $image = $api->_build_url({object_number => 'ABC', tiles => 1}, {});
my $image_uri = URI->new($image);
is($image_uri->path, '/api/en/collection/ABC/tiles', 'URL image tile path is correct');
is([$image_uri->query_param], ['key'], 'URL image tile only has key param');
is($image_uri->query_param('key'), 'testkey', 'URL image tile contains key');


done_testing;

